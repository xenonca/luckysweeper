LuckySweeper
=========
Proof of concept of a minesweeper game in Minetest.  
NOTE: This game is in a beta state, some things might not yet work properly. Expect bugs.

## Description
[Minesweeper](https://en.wikipedia.org/wiki/Minesweeper_(video_game))

## To Do
- [x] Detect when a player wins a game  
- [ ] Save play time (rankings)

## Wishlist
- [ ] Customizable board size  

## Licence
Code: LGPLv2.1 by xenonca  
Media: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by xenonca  
Mod `player_api`: see corresponding licence

## Version
0.6 Time recording  
0.5 Detect wins/Add sound  
0.4 Add licence  
0.3 Replace screenshot  
0.2 Description update  
0.1 Stable release  